'use strict';

const ws = require('ws');

const Async = require('@scola/async');
const Deep = require('@scola/deep');
const DI = require('@scola/di');
const Message = require('@scola/transport-message');

const Client = require('./lib/client');
const Connection = require('./lib/connection');

class Module extends DI.Module {
  configure() {
    this.inject(Client).with(
      this.provider(Connection),
      this.provider(Message)
    );

    this.inject(Connection).with(
      this.array([]),
      this.provider(Async.Serial),
      this.provider(Message),
      this.factory(ws),
      this.instance(Deep.Map)
    );
  }
}

module.exports = {
  Client,
  Connection,
  Module
};
