'use strict';

const EventHandler = require('@scola/events');

class Client extends EventHandler {
  constructor(connection, message) {
    super();

    this.connectionProvider = connection;
    this.messageProvider = message;
  }

  connect(options) {
    this.emit('debug', this, 'connect', options);

    this.connectionProvider
      .get()
      .once('open', this.handleOpen.bind(this))
      .open(options);
  }

  createMessage() {
    this.emit('debug', this, 'createMessage');
    return this.messageProvider.get();
  }

  handleOpen(connection) {
    this.emit('connection', connection);
  }
}

module.exports = Client;
