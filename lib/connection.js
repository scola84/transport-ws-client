'use strict';

const Abstract = require('@scola/transport-ws-common');
const Error = require('@scola/error');

class Connection extends Abstract.Connection {
  constructor(filters, serial, message, connection, options) {
    super(filters, serial, message);

    this.connectionFactory = connection;

    this.options = options.assign({
      address: null,
      binaryType: null,
      codes: [1000],
      maxAttempts: 10,
      maxInterval: 30,
      protocols: null
    });

    this.attempts = 0;
    this.timeout = null;
  }

  getOptions() {
    return this.options;
  }

  open(options) {
    this.emit('debug', this, 'open', options);
    this.options.assign(options);

    return this.handleOpen();
  }

  handleMessage(data) {
    if (Error.isError(data)) {
      return this.handleError(Error.fromString(data));
    }

    return super.handleMessage(data);
  }

  handleOpen() {
    const connection = this.connectionFactory.create(
      this.options.get('address'),
      this.options.get('protocols')
    );

    if (this.options.get('binaryType')) {
      connection.binaryType = this.options.get('binaryType');
    }

    connection.on('error', () => {
      connection.removeAllListeners();
      this.handleReopen();
    });

    connection.on('open', () => {
      connection.removeAllListeners();

      this.attempts = 0;
      this.connection = connection;
      this.bindListeners();

      this.emit('open', this);
    });

    return this;
  }

  handleClose(code, reason) {
    if (this.options.get('codes').indexOf(code) !== -1) {
      return this.handleReopen();
    }

    return super.handleClose(code, reason);
  }

  handleReopen() {
    this.emit('debug', this, 'handleReopen', this.attempts);

    clearTimeout(this.timeout);

    if (this.attempts === this.options.get('maxAttempts')) {
      return;
    }

    this.timeout = setTimeout(
      this.handleOpen.bind(this),
      this.calculateDelay()
    );

    this.attempts += 1;
  }

  calculateDelay() {
    return Math.random() * Math.min(
      this.options.get('maxInterval') * 1000,
      Math.pow(2, this.attempts) * 1000
    );
  }
}

module.exports = Connection;
