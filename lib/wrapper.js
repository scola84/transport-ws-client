'use strict';

const EventHandler = require('@scola/events');

class WebSocketWrapper extends EventHandler {
  constructor(address, protocols) {
    super();

    this.websocket = new WebSocket(address, protocols);
    this.addHandlers();
  }

  get binaryType() {
    return this.websocket.binaryType;
  }

  set binaryType(binaryType) {
    this.websocket.binaryType = binaryType;
  }

  get bufferedAmount() {
    return this.websocket.bufferedAmount;
  }

  get extensions() {
    return this.websocket.extensions;
  }

  get protocol() {
    return this.websocket.protocol;
  }

  get readyState() {
    return this.websocket.readyState;
  }

  get url() {
    return this.websocket.url;
  }

  get CONNECTING() {
    return this.websocket.CONNECTING;
  }

  get OPEN() {
    return this.websocket.OPEN;
  }

  get CLOSING() {
    return this.websocket.CLOSING;
  }

  get CLOSED() {
    return this.websocket.CLOSED;
  }

  close(code, reason) {
    this.removeHandlers();

    // Other than 1000 not allowed in browsers
    this.websocket.close(1000, reason);
    this.handleClose(code, reason);
  }

  send(data, options, callback) {
    this.websocket.send(data);

    if (callback) {
      callback();
    }
  }

  addHandlers() {
    this.websocket.onclose = this.handleClose.bind(this);
    this.websocket.onerror = this.handleError.bind(this);
    this.websocket.onmessage = this.handleMessage.bind(this);
    this.websocket.onopen = this.handleOpen.bind(this);
  }

  removeHandlers() {
    this.websocket.onclose = null;
    this.websocket.onerror = null;
    this.websocket.onmessage = null;
    this.websocket.onopen = null;
  }

  handleClose(event) {
    this.emit('close', event.code, event.reason);
  }

  handleError() {
    this.emit('error', new Error('WebSocket Error'));
  }

  handleMessage(event) {
    this.emit('message', event.data, {
      binary: typeof event.data === 'string' ? false : true,
      masked: false
    });
  }

  handleOpen() {
    this.emit('open');
  }
}

module.exports = WebSocketWrapper;
